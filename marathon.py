import http.server
import praw
import random
import urllib.parse

## USER AUTHENTICATION

def authenticate():
    reddit = praw.Reddit(client_id='uxK3FNr4VT3HfQ',
                         client_secret=None,
                         redirect_uri='http://localhost:8080',
                         user_agent='marathon v1.0 (/u/notevenalongname)')

    state = '%030x' % random.randrange(16 ** 30)
    print('Follow this link to allow marathon to access your account:', reddit.auth.url(['identity', 'mysubreddits', 'privatemessages', 'read'], state, 'temporary', implicit=True))

    class Handler(http.server.BaseHTTPRequestHandler):
        def do_GET(self):
            HTML = '''
<!DOCTYPE html>
<html>
  <head>
    <title>marathon</title>
    <script>{}</script>
  </head>
  <body>
    <p>Use the following values to authorize <code>marathon</code>:</p>
    <table>
      <tr>
        <td><label for="token">Token</label></td>
        <td width="500"><input id="token" style="width: 500px" /></td>
      </tr>
      <tr>
        <td><label for="state">State</label></td>
        <td width="500"><input id="state" style="width: 500px" /></td>
      </tr>
    </table>
  </body>
</html>'''
            JS = '''
window.onload = function () {
    var hash = window.location.hash.substr(1);
    var result = hash.split('&').reduce(function (result, item) {
        var parts = item.split('=');
        result[parts[0]] = parts[1];
        return result;
    }, {});
    document.getElementById('token').value = result['access_token'];
    document.getElementById('state').value = result['state'];
}'''
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.end_headers()
            self.wfile.write(HTML.format(JS).encode())

    with http.server.HTTPServer(('', 8080), Handler) as httpd:
        httpd.handle_request()

    print()
    token = input('Enter token: ').strip()
    valid = input('Enter state: ').strip() == state

    if not valid:
        raise ValueError('Invalid state')

    reddit.auth.implicit(token, 3600, 'identity mysubreddits privatemessages read')

    return reddit

reddit = authenticate()

print()
print('Authenticated as', reddit.user.me())
print()

subs = list(reddit.user.moderator_subreddits())
for idx, sub in enumerate(subs):
    print(' {0: >3}) {1}'.format(idx, str(sub)))

print()

sub_idx = None
while sub_idx == None:
    try:
        sub_idx = int(input('Select subreddit (by number): '))
        if sub_idx < 0 or sub_idx >= len(subs):
            sub_idx = None
    except:
        sub_idx = None
sub = subs[sub_idx]

print()
print('Selected /r/{}'.format(str(sub)))

contribs = list(sub.contributor())

print('Found {} contributors'.format(len(contribs)))
print()

subject = input('Enter the message subject: ')

print('Enter your message.')
print('Enter a line containing nothing but a dot (.) to end the message. Press CTRL+C to abort.')
print()

message = []
line = input()
while line != '.':
    message.append(line)
    line = input()
message = '\n'.join(message)

print()
confirm = ''
while confirm.lower() != 'yes':
    confirm = input('Enter "yes" to begin sending messages: ')
print()

length = len(str(len(contribs)))
count = str(len(contribs))
index = 1
for contrib in contribs:
    print("[{} / {}] {}".format(str(index).rjust(length), count, str(contrib)))
    contrib.message(subject, message, from_subreddit=sub)
    index += 1
